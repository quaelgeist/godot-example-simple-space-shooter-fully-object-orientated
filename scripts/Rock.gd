extends RigidBody2D

const SPEED = 80

var ship_class = preload("res://scripts/Ship.gd")
var laser_class = preload("res://scripts/Laser.gd")

func _integrate_forces(s):

	var lv = s.get_linear_velocity()
	var step = s.get_step()
	
	
	for i in range(s.get_contact_count()):
		var cc = s.get_contact_collider_object(i)
		if cc:
			if (cc extends laser_class) || (cc extends ship_class):
				self.get_node("Anim").play("explode")
				self.clear_shapes()

	#move down
	lv.y += SPEED * step
	s.set_linear_velocity(lv)
	
	if rock_is_over_screen() == true:
		_die()
	
func _init():
	self.set_pos(Vector2(rand_range(0,OS.get_video_mode_size().x), 0))

func _die():
	print("Rock deleted")
	queue_free()

func rock_is_over_screen():
	if self.get_pos().y > OS.get_video_mode_size().y :
		return true
	else:
		return false
