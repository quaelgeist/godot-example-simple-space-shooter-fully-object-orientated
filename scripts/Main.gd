extends Node2D

###
###This is an example.
###Its meant to be small, clean and understandable
###This is not meant to be productive code
###


var last_rock_time = OS.get_unix_time()
const ROCK_SPAWN_TIME = 1

var ship

func _ready():
	ship = self.get_node("Ship")
	set_process(true)

func _process(delta):
	ship.shot()
	
	#this will be called every second. Normally one would use a timer-node for this!
	if OS.get_unix_time() - last_rock_time > ROCK_SPAWN_TIME:
		add_rock()
		last_rock_time = OS.get_unix_time()

func add_rock():
	#print("Add rock")
	var rock = preload("res://scenes/Rock.scn").instance()
	add_child(rock)