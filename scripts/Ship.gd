extends RigidBody2D

const SPEED = 200
const MAX_SPEED = 800
var STOP = Vector2(0,0)
var health = 1

const HIT_DELAY = 3 #seconds
var last_hit = OS.get_unix_time()
var screensize = OS.get_video_mode_size()

#const LASER_SPAWN_TIME = 1/10
#var last_laser_time = OS.get_unix_time()
var fire_semaph = false

var rock_class = preload("res://scripts/Rock.gd")
var laser_scene = preload("res://scenes/Laser.scn")

var ship_messurement #assigned in runtime

func _integrate_forces(s):
	var lv = s.get_linear_velocity()
	var step = s.get_step()	
	
	s.set_linear_velocity(movement(lv))
	
	shoot()
	collider(s)

func movement(lv):

	var move_left = false
	var move_right = false
	var move_up = false
	var move_down = false
	var pos = self.get_global_pos()
	
	#Parse the controls
	if Input.is_action_pressed("ui_left") and pos.x > 0 + ship_messurement.x/2:
		move_left = true
	if Input.is_action_pressed("ui_right") and pos.x < screensize.x - ship_messurement.x/2:
		move_right = true
	if Input.is_action_pressed("ui_up") and pos.y > 0 + ship_messurement.y/2:
		move_up = true
	if Input.is_action_pressed("ui_down") and pos.y < screensize.y - ship_messurement.y/2:
		move_down = true
	
	#Actual Movement
	if move_up && !(move_down) && lv.y > -MAX_SPEED:
		lv.y -= SPEED
	if move_down and !(move_up) and lv.y < MAX_SPEED:
		lv.y += SPEED
	if move_left and !(move_right) and lv.x > -MAX_SPEED:
		lv.x -= SPEED
	if move_right and !(move_left) and lv.x < MAX_SPEED:
		lv.x += SPEED
			
	#Breaking / Damping
	if !(move_left) and !(move_right):
		if lv.x > 0:
			lv.x -= SPEED
		elif lv.x < 0 :
			lv.x += SPEED
	if !(move_up) and !(move_down):
		if lv.y > 0:
			lv.y -= SPEED
		elif lv.y < 0:
			lv.y += SPEED

	return lv


#when the ship colides with a rock
func collider(s):
		
	for i in range(s.get_contact_count()):
#		print("collision")
		var cc = s.get_contact_collider_object(i)
		if cc:
			if cc extends rock_class: # when the colision object is a rock
				self.get_node("Anim").play("explode")
				self.clear_shapes()

				
func get_damage():
	if OS.get_unix_time() - last_hit >= HIT_DELAY :
		health -=1
		if health <= 0:
			_die()
		last_hit = OS.get_unix_time()

func shoot():
	if Input.is_action_pressed("shoot") and fire_semaph == false:
		#print("pressed")
		var laser = laser_scene.instance()
		var pos = laser.get_pos()
		pos.y -= ship_messurement.y/2 + 10 #toleranze
		
		laser.set_pos(pos)
		add_child(laser)
		#last_laser_time = OS.get_unix_time()
		fire_semaph = true
	if !(Input.is_action_pressed("shoot")) and fire_semaph == true:
		#print("released")
		fire_semaph = false

func _die():
	print("Game Over!")
	queue_free()

func _ready():
	ship_messurement = Vector2(self.get_node("Sprite").get_texture().get_width(),self.get_node("Sprite").get_texture().get_height())