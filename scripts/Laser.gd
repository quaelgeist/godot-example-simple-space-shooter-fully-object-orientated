extends RigidBody2D

const SPEED = 20
var rock_class = preload("res://scripts/Rock.gd")

func _die():
	#print("laser removed")
	queue_free()

func _integrate_forces(s):
	move()
	collider(s)
	
	if is_laser_out_of_screen():
		_die()

func collider(s):
	for i in range(s.get_contact_count()):
		var cc = s.get_contact_collider_object(i)
		
		if cc:
			if cc extends rock_class:
				_die()

func move():
	var current_pos = self.get_global_pos()
	current_pos.y -= SPEED
	self.set_global_pos(current_pos)
		
func is_laser_out_of_screen():
	if self.get_global_pos().y <= 0:
		return true
	else:
		return false