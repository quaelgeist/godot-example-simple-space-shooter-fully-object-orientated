# README #

This is an example "top-down-shooter" made in the Godot game engine. Visit http://www.godotengine.org/ for further information.

This example displays how to make a clean and object orientated games structure in Godot.
Topics in this example are:

* 8-Way 2D-Movement
* Controlling of a player-character 
* Spawning of enemies and which can destroy them (Rocks and Lasers)
* 2D-physics and -collision detection
* Simple particle animation
* Dynamic and Object Orientated game structure

### How do I get set up? ###

* Install the Godot Game engine
* Clone the projekt to your harddrive ("git clone" or "hg clone")
* Import the projekt in your Godot game engine

### Who do I talk to? ###

* This example is made for people who want to learn game-development in Godot.
* This example may help programming beginners to understand the way of object orientated programming.

### Licensing and Information ###

Author: Sean Engelhardt <sean.f.t.engelhardt@gmail.com>

Licence: GPL >= v2